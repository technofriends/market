import os
from mimetypes import guess_type
from itertools import chain

from django.conf import settings
from django.core.servers.basehttp import FileWrapper
from django.db.models import Q
from django.forms.models import modelformset_factory
from django.shortcuts import render_to_response, RequestContext, Http404, HttpResponseRedirect,HttpResponse, get_object_or_404
from django.template.defaultfilters import slugify



from .models import Product, Category, ProductImage
from .forms import ProductForm,ProductImageForm
from cart.models import Cart,CartItem


# from .models import ProductImage

def check_product(user,product):

	if product in user.userpurchase.products.all():
		return True
	else:
		return False

def download_product(request,slug,filename):

	product = Product.objects.get(slug=slug)

	# check if the user has purchased the product

	if product in request.user.userpurchase.products.all():
		print "product is here"
		product_file = str(product.download)
		file_path = os.path.join(settings.PROTECTED_UPLOADS, product_file)
		# print file_path
		wrapper = FileWrapper(file(file_path))
		response = HttpResponse(wrapper, content_type=guess_type(product_file))
		
		# set response headers

		response['Content-Disposition'] = 'attachment;filename=%s' %filename
		response['Content-Type'] = ''
		response['X-SendFile'] = file_path

		return response
	else:
		raise Http404

	# return render_to_response('products/all.html', locals(),context_instance=RequestContext(request))


def list_all(request):

	products = Product.objects.filter(active=True)
	
	return render_to_response('products/all.html', locals(),context_instance=RequestContext(request))

def add_product(request):

	form = ProductForm(request.POST or None)
	
	if form.is_valid():
		product = form.save(commit=False)
		product.user = request.user
		product.slug = slugify(form.cleaned_data['title'])
		product.active = False

		product.save()
		return HttpResponseRedirect('/products/%s'%(product.slug))

	return render_to_response('products/edit.html', locals(),context_instance=RequestContext(request))



def edit_product(request,slug):
	instance = Product.objects.get(slug=slug)
	
	if request.user == instance.user:

		# populating the form with the product details for editing. 
		# this is done by passing an instance parameter. 
		# here, we are passing instance value as the product instance obtained above. 

		form = ProductForm(request.POST or None, instance=instance)

		if form.is_valid():

			product_edit = form.save(commit=False)
			product_edit.save()

		return render_to_response('products/edit.html', locals(),context_instance=RequestContext(request))

	else:
		raise Http404


def manage_product_image(request,slug):
	try:
		product = Product.objects.get(slug=slug)
	except:
		product = False

	if request.user == product.user:
		ProductImageFormset = modelformset_factory(ProductImage, form=ProductImageForm, can_delete=True)
		queryset = ProductImage.objects.filter(product__slug=slug)
		formset = ProductImageFormset(request.POST or None, request.FILES or None, queryset=queryset)
		form = ProductImageForm(request.POST or None)

		if formset.is_valid():
			for form in formset:
				instance = form.save(commit=False)
				instance.product = product
				instance.save()
			if formset.deleted_forms:
				formset.save()

		return render_to_response('products/manage_images.html', locals(),context_instance=RequestContext(request))
	
	else:
		raise Http404




def single(request,slug):

	product = Product.objects.get(slug=slug)
	# images = ProductImage.objects.filter(product=product)
	images = product.productimage_set.all().filter(product=product)
	categories = product.category_set.all()
	
	if request.user.is_authenticated():
		downloadable = check_product(request.user, product)
	else:
		downloadable = False

	try:
		cart_id = request.session['cart_id']
		cart = Cart.objects.filter(id=cart_id)
		cart_item = CartItem.objects.filter(cart=cart)
		for item in cart_item: 
			if item.product == product:
				in_cart = True
	
	except:
		in_cart = False

	if request.user == product.user:
		edit_allowed = True

	
	if len(categories) >= 1:
		related_products = []
		for category in categories:
			rel_prds = category.products.all()
			for prod in rel_prds:
				if not prod == product:
					related_products.append(prod)


	
	return render_to_response('products/single.html', locals(),context_instance=RequestContext(request))


def search(request):

	try:
		q = request.GET.get('q','')
		query = q

		k = q.split()
		print "k is " + str(k)

		if len(k) <=2:

			product_queryset = Product.objects.filter(
				Q(title__icontains=q)|
				Q(description__icontains=q)
				)
			category_queryset = Category.objects.filter(
				Q(title__icontains=q)|
				Q(description__icontains=q)
				)

		products = list(chain(product_queryset,category_queryset))


	except:
		q = False


	return render_to_response('products/search.html', locals(),context_instance=RequestContext(request))


def category_single(request,slug):

	try:
		category = Category.objects.get(slug=slug)
	except:
		raise Http404

	products = category.products.all()
	related = []
	for item in products:
		product_categories = item.category_set.all()
		print product_categories
		for single_category in product_categories:
			if not single_category == category:
				related.append(single_category)


	return render_to_response('products/category.html', locals(),context_instance=RequestContext(request))




