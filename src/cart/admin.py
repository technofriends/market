from django.contrib import admin
from .models import Cart, CartItem


class CartItemInline(admin.TabularInline):
	model = CartItem

class CartAdmin(admin.ModelAdmin):
	save_on_top = True

	inlines = [CartItemInline]

	class Meta:
		model = Cart

admin.site.register(Cart,CartAdmin)