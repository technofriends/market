import os
from django.conf import settings
from django.shortcuts import render_to_response, RequestContext, Http404, HttpResponseRedirect,HttpResponse, get_object_or_404

from products.models import Featured

def home(request):

	featured = Featured.objects.all()[0]
	featured_products=[]

	for item in featured.products.all():
		featured_products.append(item)


	
	return render_to_response('home.html', locals(),context_instance=RequestContext(request))

