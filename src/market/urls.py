from django.conf.urls import patterns, include, url

from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.STATIC_ROOT
		}),
	(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT
		}),
    url(r'^$','market.views.home',name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^cart/', include('cart.urls')),
    url(r'^products/', include('products.urls')),
    url(r'^accounts/profile/','market.views.home'),
    url(r'^lib/','profiles.views.library',name='library'),
    url(r'^accounts/', include('allauth.urls')),

)
