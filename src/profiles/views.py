import os
from mimetypes import guess_type

from django.conf import settings
from django.shortcuts import render_to_response, RequestContext, Http404, HttpResponseRedirect,HttpResponse
from django.template.defaultfilters import slugify
from django.forms.models import modelformset_factory
from django.core.servers.basehttp import FileWrapper


from products.models import Product
from .models import UserPurchase

def library(request):
	if request.user.is_authenticated():
		print "user is authenticated" + str(request.user.username)

		user_purchases = UserPurchase.objects.get(user=request.user)
		products = user_purchases.products.all()
		# products = request.user.userpurchase.products.all()  # this is same as above. 

		return render_to_response('profiles/library.html',locals(),context_instance=RequestContext(request))
	
	else:
		return HttpResponseRedirect('/accounts/login')
